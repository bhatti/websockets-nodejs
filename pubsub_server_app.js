////////////////////////////////////////////////////////////////////////////////////////////
//
// Node server app for pub/sub model
//
//////////////////////////////////////////////////////////////////////////////////////////// 
// 
// Setup configuration
var OUTLIER_SIZE = process.env.OUTLIER_SIZE || 5;
var MAX_THREADS = process.env.MAX_THREADS || 20;
var MAX_ROWS = process.env.MAX_ROWS || 5;
var SEND_MESSAGE_INTERVAL = SEND_MESSAGE_INTERVAL || 100;
var useSocketIO = false;

var nextClientId = 0;
var errors = 0;
//
var pubsubController = require('pubsub_controller')(MAX_ROWS)
var cluster = require('cluster');
var hwUsage = require('hardware_usage');
var metrics = require('metrics')(OUTLIER_SIZE);
hwUsage.start();

if (cluster.isMaster) {
   for (var i = 0; i < MAX_THREADS; i++) {
      cluster.fork();
   }
   cluster.on('exit', function(worker, code, signal) {
      //console.log('worker ' + worker.process.pid + ' died');
      //if (code != 0) {
      //   cluster.fork();
      //}
   });
} else {
   if (useSocketIO) {
      var io = require('socket.io').listen(8124);
      io.set('log level', 1); 
      io.set('transports', ['websocket']);
      io.set('force new connection', true);
      io.enable('browser client gzip');

      io.sockets.on('connection', function(socket) {
         socket.key = socket.handshake.address.address + ':' + socket.handshake.address.port + ':' + (++nextClientId);
         pubsubController.subscribe(socket);
         socket.on('disconnect', function() { 
            pubsubController.unsubscribe(socket);
         });
         
         socket.on('end', function() { 
         });
         socket.on('error', function() { 
            errors++;
         });
      });
   } else {
      var WebSocketServer = require('ws').Server
        , wss = new WebSocketServer({port: 8124});
      wss.on('connection', function(ws) {
          ws.key = ++nextClientId;
          pubsubController.subscribe(ws);
          ws.on('close', function() {
           });
          ws.on('error', function() {
            errors++;
          });
      });
   } 

   setInterval(function() {
      pubsubController.pushUpdates();
   }, SEND_MESSAGE_INTERVAL);
}

setInterval(function() {
   hwUsage.stop(function(usage) {
      console.log('clients,' + hwUsage.heading() + ',' + metrics.heading());
      console.log(nextClientId+ ',' + usage.toString() + ',' + metrics.summary().toString());
   });
}, 15000);

process.on('uncaughtException', function(err) {
  console.log("GLOBAL " + err + "\n" + err.stack);
  //console.trace('stack trace');
  //process.exit(1);
});
