#!/bin/bash 

export MAX_MESSAGES=10000
export OUTLIER_SIZE=10
threads=5
connections_per_thread=5000
#ulimit -n 999999
while [ $threads -lt 10 ]
do
   while [ $connections_per_thread -lt 50000 ]
   do
      export MAX_THREADS=$threads 
      export MAX_CONNECTIONS_PER_THREAD=$connections_per_thread  
      echo starting node with $threads threads and $connections_per_thread connections
      node -max-old-space-size=8192 -nouse-idle-notification pubsub_client_app.js
      connections_per_thread=`expr $connections_per_thread + 500|awk '{print $1}'`
   done 
   threads=`expr $threads + 1|awk '{print $1}'`
done
echo all done
