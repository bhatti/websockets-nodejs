////////////////////////////////////////////////////////////////////////////////////////////
//
// Node server app for order requests
//
//////////////////////////////////////////////////////////////////////////////////////////// 
// 
// Setup configuration 
// sudo sysctl -w net.core.somaxconn=10000
// sudo sysctl -w net.ipv4.tcp_max_syn_backlog=10000 
// net.core.rmem_max = 33554432
// net.core.wmem_max = 33554432
// net.ipv4.tcp_rmem = 4096 16384 33554432
// net.ipv4.tcp_wmem = 4096 16384 33554432
// net.ipv4.tcp_mem = 786432 1048576 26777216
// net.ipv4.tcp_max_tw_buckets = 360000
// net.core.netdev_max_backlog = 2500
// vm.min_free_kbytes = 65536
// vm.swappiness = 0
// net.ipv4.ip_local_port_range = 1024 65535
//
// Edit /etc/sysctl.conf
// fs.file-max = 100000
//   
// Edit /etc/limits.conf
// sbhatti         soft    nofile          320000
// sbhatti         hard    nofile          327680
//
var OUTLIER_SIZE = process.env.OUTLIER_SIZE || 2;
var MAX_THREADS = process.env.MAX_THREADS || 2;
var useSocketIO = false;

var cluster = require('cluster');
var hwUsage = require('hardware_usage');
var metrics = require('metrics')(OUTLIER_SIZE);
var fs = require('fs');

hwUsage.start();
var totalClients = 0;
var totalErrors = 0;


if (cluster.isMaster) {
   for (var i = 0; i < MAX_THREADS; i++) {
      cluster.fork();
   }
   cluster.on('exit', function(worker, code, signal) {
      //console.log('worker ' + worker.process.pid + ' died');
      //if (code != 0) {
      //   cluster.fork();
      //}
   });
} else {
   //
   if (useSocketIO) {
      var io = require('socket.io').listen(8124);
      io.set('log level', 1); 
      io.set('transports', ['websocket']);
      io.set('force new connection', true);
      io.enable('browser client gzip');
      io.sockets.on('connection', function(socket) {
         totalClients++;
         socket.address = socket.handshake.address.address + ':' + socket.handshake.address.port;
         socket.on('order-request', function(request) {
            if (typeof request !== "undefined") {
               var response = request;
               if (typeof request['request'] !== "undefined") {
                  metrics.update(request.request, request.timestamp, JSON.stringify(request).length);
               }
               socket.emit('order-response', response);
            }
         });

         // disconnected
         socket.on('disconnect', function() { 
            totalClients--;
         });
         
         socket.on('end', function() { 
         });
         socket.on('error', function() {
            totalErrors++;
         });
      });
   } else {
      var WebSocketServer = require('ws').Server
        , wss = new WebSocketServer({port: 8124});
      wss.on('connection', function(ws) {
          totalClients++;
          ws.on('message', function(text) {
            var request = JSON.parse(text);
            if (typeof request['request'] !== "undefined") {
               metrics.update(request.request, request.timestamp, JSON.stringify(request).length);
            }
            var response = text;
            ws.send(response);
         });
         ws.on('close', function() {
            totalClients--;
           });
         ws.on('error', function() {
            totalErrors++;
         });
      });
   } 


   var logpath = "worker_" + cluster.worker.id + ".log";
   var fd = fs.openSync(logpath, 'w', 0644);
   fs.writeSync(fd, 'clients, errors, ' + hwUsage.heading() + ',' + metrics.heading() + '\n');
   setInterval(function() {
       hwUsage.stop(function(usage) {
         fs.writeSync(fd, totalClients + ',' + totalErrors + ',' + usage.toString() + ',' + metrics.summary().toString() + '\n');
       });
    }, 15000);
}


process.on('uncaughtException', function(err) {
  console.log("GLOBAL " + err + "\n" + err.stack);
  // console.trace('stack trace');
  // process.exit(1);
});
