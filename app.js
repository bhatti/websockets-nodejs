////////////////////////////////////////////////////////////////////////////////////////////
//
// Default node server app
//
////////////////////////////////////////////////////////////////////////////////////////////
var express = require('express')
   , sio = require('socket.io')
   , http = require('http')
   , ohclient = require('oh_client_shared')
   , app = express();

var stockQuoteSubscriptions = require('quote_subscriptions')(ohclient);
var chainQuoteSubscriptions = require('chain_subscriptions')(ohclient);
var alertSubscriptions = require('alert_subscriptions')(ohclient);
var activitySubscriptions = require('activity_subscriptions')(ohclient);

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var server = http.createServer(app);
var sequence = 0;

app.configure(function () { 
   app.use(express.static(__dirname + '/public')); 
   app.use(app.router);
})

var io = sio.listen(server);
//io.set('log level', 1);
server.listen(8124);

io.sockets.on('connection', function(socket) {
   // subscribe to stock quotes 
   socket.on('subscribe-stock-quote', function(symbol) {
      console.log('subscribe-stock-quote received');
      stockQuoteSubscriptions.subscribe(socket, symbol, function() {
         socket.emit('subscribed-stock-quote', 'You have been subscribed to ' + symbol); 
      });
   });

   // unsubscribe to stock quotes 
   socket.on('unsubscribe-stock-quote', function (symbol) {
      console.log('unsubscribe-stock-quote received');
      stockQuoteSubscriptions.unsubscribe(socket, symbol, function() {
         socket.emit('unsubscribed-stock-quote received', 'You have been unsubscribed to ' + symbol);
      });
   });

   // subscribe to option chain quotes
   socket.on('subscribe-chain-quote', function(symbol) {
      console.log('subscribe-chain-quote received');
      chainQuoteSubscriptions.subscribe(socket, symbol, function() {
         socket.emit('subscribed-chain-quote', 'You have been subscribed to ' + symbol); 
      });
   });

   // unsubscribe to option chain quotes
   socket.on('unsubscribe-chain-quote', function (symbol) {
      console.log('unsubscribe-chain-quote received');
      chainQuoteSubscriptions.unsubscribe(socket, symbol, function() {
         socket.emit('unsubscribed-chain-quote received', 'You have been unsubscribed to ' + symbol);
      });
   });


   // subscribe to alerts
   socket.on('subscribe-alerts', function() {
      console.log('subscribe-alerts received');
      alertSubscriptions.subscribe(socket, function() {
         socket.emit('subscribed-alerts', 'You have been subscribed to alerts'); 
      });
   });

   // unsubscribe to alerts
   socket.on('unsubscribe-alerts', function () {
      console.log('unsubscribe-alerts received');
      alertSubscriptions.unsubscribe(socket, function() {
         socket.emit('unsubscribed-alerts received', 'You have been unsubscribed to alerts');
      });
   });

   // subscribe to activities
   socket.on('subscribe-activities', function() {
      console.log('subscribe-activities received');
      activitySubscriptions.subscribe(socket, '1', function() {
         socket.emit('subscribed-activities', 'You have been subscribed to activities'); 
      });
   });

   // unsubscribe to activities
   socket.on('unsubscribe-activities', function () {
      console.log('unsubscribe-activities received');
      activitySubscriptions.unsubscribe(socket, '1', function() {
         socket.emit('unsubscribed-activities received', 'You have been unsubscribed to activities');
      });
   });


   // disconnected
   socket.on('disconnect', function() { 
      stockQuoteSubscriptions.unsubscribeAll(socket, function() {
         socket.emit('disconnected', socket.symbol + ' has been unsubscribed from stock quotes.');
      });
      chainQuoteSubscriptions.unsubscribeAll(socket, function() {
         socket.emit('disconnected', socket.symbol + ' has been unsubscribed from chain quotes.');
      });
      alertSubscriptions.unsubscribeAll(socket, function() {
         socket.emit('disconnected', 'Client has been unsubscribed from alerts.');
      });
      activitySubscriptions.unsubscribeAll(socket, function() {
         socket.emit('disconnected', 'Client has been unsubscribed from activities.');
      });
   });
});

// update clients with latest stock quotes
setInterval(function () {
   stockQuoteSubscriptions.pushUpdates();
   io.sockets.emit('sequence', ++sequence);
}, 500);

// update clients with latest option chain quotes
setInterval(function () {
   chainQuoteSubscriptions.pushUpdates();
}, 800);

// update clients with latest alerts
setInterval(function () {
   alertSubscriptions.pushUpdates(io);
}, 1000);

// update clients with activities
setInterval(function () {
   activitySubscriptions.pushUpdates();
}, 1000);


process.on('uncaughtException', function(err) {
  console.log("GLOBAL " + err + "\n" + err.stack);
  process.exit(1);
});


