#!/bin/bash 

clients=100
while [ $clients -lt 100000 ]
do
   interval=100
   while [ $interval -lt 1000 ]
   do
      export MAX_CLIENTS=$clients
      export SEND_MESSAGE_INTERVAL=$interval
      echo starting node with $clients clients and $interval interval
      ##node -max-old-space-size=8192 -nouse-idle-notification order_client_app.js
      interval=`expr $interval + 100|awk '{print $1}'`
   done 
   clients=`expr $clients + 100|awk '{print $1}'`
done
echo all done
