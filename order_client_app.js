////////////////////////////////////////////////////////////////////////////////////////////
//
// Test client for execution report that creates multiple threads and multiple connections 
// within each thread to hit the server and measure response time.
//
////////////////////////////////////////////////////////////////////////////////////////////
//
// Configuring test from environment variables
//
var OUTLIER_SIZE = process.env.OUTLIER_SIZE || 5;
var MAX_CLIENTS = process.env.MAX_CLIENTS || 100;
var SEND_MESSAGE_INTERVAL = process.env.SEND_MESSAGE_INTERVAL || 100;
var MAX_RUNTIME = (process.env.MAX_RUNTIME_MIN || 5) * 60 * 1000;
var MAX_ROWS = process.env.MAX_ROWS || 5;
var MAX_ERRORS = process.env.MAX_ERRORS || 2000;


var fs = require('fs');
var hwUsage = require('hardware_usage');
var metrics = require('metrics')(OUTLIER_SIZE);
hwUsage.start();

var order_client = require('order_client')(metrics, hwUsage, MAX_CLIENTS, SEND_MESSAGE_INTERVAL);


var buildClients = function() {
   if (order_client.totalErrors() >= MAX_ERRORS) {
      console.log('too many errors');
      process.exit(1);
      return;
   }
   if (order_client.totalConnected() >= MAX_CLIENTS) {
      return;
   }
   for (var i=0; i<2; i++) {
      try {
         var client = order_client.newClient();
         client.setupConnectListener();
         client.setupResponseListener();
         setInterval(function() {
            try {
               client.sendRequest(MAX_ROWS);
            } catch (err) {
               console.log("Error: " + err);
            }
         }, SEND_MESSAGE_INTERVAL);
      } catch (err) {
         console.log("Error: " + err);
      }
   }
};


setInterval(buildClients, 10);

var fd = fs.openSync('client_' + MAX_CLIENTS + '_' + SEND_MESSAGE_INTERVAL + '.log', 'w', 0644);
fs.writeSync(fd, order_client.logHeading());

setInterval(function() {
   order_client.logMessage(function(msg) {
      fs.writeSync(fd, msg);
   });
}, 15000);

process.on('uncaughtException', function(err) {
  console.log("GLOBAL " + err + "\n" + err.stack);
  console.trace('stack trace');
  //process.exit(1);
});

setTimeout(function() {
   console.log('ran out of time');
   process.exit(0);
}, MAX_RUNTIME);
