////////////////////////////////////////////////////////////////////////////////////////////
//
// Test client for pub/sub model that creates multiple threads and multiple connections 
// within each thread to hit the server and measure response time.
//
////////////////////////////////////////////////////////////////////////////////////////////
//
// Configuring test from environment variables
//
var OUTLIER_SIZE = process.env.OUTLIER_SIZE || 2;

var hwUsage = require('hardware_usage');
var metrics = require('metrics')(OUTLIER_SIZE);
hwUsage.start();

var client = require('pubsub_client')(metrics, hwUsage);
var clients = [];
//


// create connections for this client
var buildClients = function() {
   for (var i=0; i<100; i++) {
      var c = client.newClient(i+0);
      clients.push(c);
   }
   hwUsage.stop(function(usage) {
      console.log('clients,connected,', hwUsage.heading() + ',' + metrics.heading());
      console.log(clients.length + ',' + client.totalConnected() + ',' + usage.toString() + ',' + metrics.summary().toString());
   });
};


setInterval(buildClients, 15000);


process.on('uncaughtException', function(err) {
  console.log("GLOBAL " + err + "\n" + err.stack);
  console.trace('stack trace');
  //process.exit(1);
});
